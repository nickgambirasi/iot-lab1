import numpy as np

def set_tensor(interpreter, image):

    idx = interpreter.get_input_details()[0]['index']
    tensor = interpreter.tensor(idx)()[0]
    tensor[:, :] = image

def output_to_tensor(interpreter, idx):

    output_details = interpreter.get_output_details()[idx]
    tensor = np.squeeze(interpreter.get_tensor(output_details['index']))
    return tensor

def detect(interpreter, img, labels):

    set_tensor(interpreter=interpreter, image=img)
    interpreter.invoke()

    boxes = output_to_tensor(interpreter, 0)
    classes = output_to_tensor(interpreter, 1)
    classes = [labels[x] for x in classes]
    scores = output_to_tensor(interpreter, 2)
    n = output_to_tensor(interpreter, 3)

    return n, boxes, classes, scores

def evaluate_predictions(boxes, classes, scores, thresh):

    for box, cl, score in zip(boxes, classes, scores):

        if cl == 'stop sign' and score > thresh:

            return -1
        
        else:

            continue

    return 0