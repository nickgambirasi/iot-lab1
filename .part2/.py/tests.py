from advanced_mapping import *
from model import *
from path_planning_and_nav import *
from model_labels import *
from object_detection import *

import picamera
import picamera.array

import cv2

from argparse import ArgumentParser

def ultrasonic_scan_test() -> np.ndarray:

    case = scan_to_array(angles=np.arange(-60, 60, 0.5))
    case = case*255

    cv2.imshow("Obstacle Map", case)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def path_search_test():

    mat = scan_to_array(angles=np.arange(-60, 60, 1))

    start = (0, 50)
    end = (99, 50)

    pth = a_star(mat, start, end)

    for pt in pth:

        mat[pt[1], pt[0]] = 0.5

    mat = mat*255
    cv2.imshow("obstacles + map", mat)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    print(pth)

def path_and_directions_test():

    mat = scan_to_array(angles=np.arange(-60, 60, 1))

    start = (0, 50)
    end = (99, 50)

    pth = a_star(mat, start, end)

    print(pth)

    directions = get_turn_list(path=pth)
    print(directions)

    for pt in pth:

        mat[pt[1], pt[0]] = 0.5

    mat = mat*255
    cv2.imshow("obstacles + map", mat)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def model_test():

    labels = get_labels()
    model = create_model()

    TEST_DIR = os.path.join(os.getcwd(), os.pardir, '.test')
    test_images = [os.path.join(TEST_DIR, x) for x in os.listdir(TEST_DIR) if x.endswith('.jpg')]

    for image in test_images:

        # read in image
        i = cv2.imread(image)

        # resize and recolor
        i_rgb = cv2.cvtColor(i, cv2.COLOR_BGR2RGB)
        i = cv2.resize(i_rgb, (320, 320))

        _, boxes, classes, scores = detect(model, i, labels)

        # see if model can recognize a stop sign
        flag = evaluate_predictions(boxes, classes, scores, 0.4)

        # output if flag was detected
        if flag == - 1:

            print("SIGN DETECTED!!!")

        else:

            continue

def camera_and_model_test():

    model = create_model()
    model.allocate_tensors()

    labels = get_labels()

    with picamera.PiCamera(resolution=(1920, 1088)) as camera:

        with picamera.array.PiRGBArray() as output:

            output.truncate(0)

            camera.capture(output, 'bgr')
            image = output.array

            cv2.imshow("live feed", image)

            # convert image to rgb for tflite

            image_tf = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image_tf = cv2.resize(image_tf, (320, 320))

            _, boxes, classes, scores = detect(model, image_tf, labels)

            flag = evaluate_predictions(boxes, classes, scores, 0.5)

            if flag == -1:
                
                print("STOP SIGN DETECTED!!!")


if __name__ == '__main__':

    parser = ArgumentParser()
    subparsers = parser.add_subparsers()

    # subparser to run the ultrasonic scan test
    parser_scan = subparsers.add_parser('scan', help='test ultrasonic scanner')
    parser_scan.set_defaults(func=ultrasonic_scan_test)

    # subparser to test mapping functionality
    parser_map = subparsers.add_parser('map', help='test mapping functionality')
    parser_map.set_defaults(func=path_search_test)

    parser_directions = subparsers.add_parser('directions', help='test directions functionality')
    parser_directions.set_defaults(func=path_and_directions_test)

    parser_model = subparsers.add_parser('model', help='Test model functionality on random images')
    parser_model.set_defaults(func=model_test)

    parser_camera_model = subparsers.add_parser('cam', help='Test camera and model functionality')
    parser_camera_model.set_defaults(func=camera_and_model_test)

    opts = parser.parse_args()

    opts.func()

