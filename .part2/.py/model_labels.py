import os
from git import Repo

def get_labels():

    try:

        print("Attempting to load labels from local source...\n")

        coco_map = {}

        i = 0
        with open(os.path.join(os.getcwd(), os.pardir, '.coco/coco-labels/coco-labels-paper.txt'), 'r') as l:

            for line in l:

                coco_map.update({i: str(line.strip())})
                i += 1

        print("Labels successfully loaded...\n")

        return coco_map

    except Exception as e:
        
        print("Could not find local version of COCO labels file...grabbing from GitHub")
        Repo.clone_from("https://github.com/amikelive/coco-labels", os.path.join(os.getcwd(), os.pardir, '.coco/coco-labels'))
        print(f"Labels file saved locally at {os.path.abspath(os.path.join(os.getcwd(), os.pardir, '.coco/coco-labels/coco-labels-paper.txt'))}")
        coco_map = {}
        i = 0
        with open(os.path.join(os.getcwd(), os.pardir, '.coco/coco-labels/coco-labels-paper.txt'), 'r') as l:

            for line in l:

                coco_map.update({i: str(line.strip())})
                i += 1

        print("Labels successfully loaded...\n")

        return coco_map
    