from turtle import forward
import numpy as np
from pathfinding.finder.a_star import AStarFinder
from pathfinding.core.grid import Grid
from pathfinding.core.diagonal_movement import DiagonalMovement

def a_star(maze, start, end):

    """
    This algorithm comes from the pathfinding Python package. It takes
    a binary numpy array where 1 indicates safe and 0 indicates obstacle.

    It returns the list of vertices travelled in the shortest path.
    """

    grid = Grid(matrix = maze)
    start = grid.node(start[0], start[1])
    end = grid.node(end[0], end[1])

    finder = AStarFinder(diagonal_movement=DiagonalMovement.never)
    path, runs = finder.find_path(start, end, grid)

    return path

def get_turn_list(path):

    """
    The goal of this function is to create a set of turns that the car
    needs to make
    """

    directions = []

    # from the path, create a list of turns needed by the car
    direction = 0
    
    for idx, item in enumerate(path):

        try:

            cur = item
            nxt = path[idx + 1]

            if direction == 0:

                if nxt[1] - cur[1] == 0:

                    continue

                elif nxt[1] - cur[1] == 1:

                    directions.append('right')
                    direction = 90

                elif nxt[1] - cur[1] == -1:

                    directions.append('left')
                    direction = -90

            if direction == -90:

                if nxt[0] - cur[0] == 0:

                    continue

                if nxt[0] - cur[0] == 1:

                    directions.append('right')
                    direction = 0

                elif nxt[0] - cur[0] == -1:

                    directions.append('left')
                    direction = 180

            if direction == 90:

                if nxt[0] - cur[0] == 0:
                    
                    continue

                elif nxt[0] - cur[0] == 1:

                    directions.append('left')
                    direction = 0

                elif nxt[0] - cur[0] == -1:

                    directions.append('right')
                    direction = 180

            if direction == 180:

                if nxt[1] - cur[1] == 0:

                    continue

                elif nxt[1] - cur[1] == -1:

                    directions.append('right')
                    direction = -90

                elif nxt[1] - cur[1] == 1:

                    directions.append('left')
                    direction = 90

        except Exception as e:

            continue

    return directions

def main():

    maze = [[1, 1, 0, 0, 0, 1, 1, 0],
            [1, 1, 1, 0, 0, 1, 1, 1],
            [0, 0, 1, 0, 0, 0, 1, 1],
            [0, 0, 1, 1, 1, 1, 0, 1],
            [1, 1, 0, 0, 1, 1, 1, 1]]

    grid = Grid(matrix=maze)
    start = grid.node(0, 0)
    end = grid.node(7, 1)

    finder = AStarFinder(diagonal_movement=DiagonalMovement.never)
    path, runs = finder.find_path(start, end, grid)

    print(path)

    directions = get_turn_list(path)

    print(directions)

if __name__ == '__main__':

    main()