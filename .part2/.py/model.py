"""
@Notice of Interaction with Third Party Software

This computer program interacts with various third-party open-source
software under their respective licenses. For more information, see
conda list.
"""
import tflite_runtime
from tflite_runtime import interpreter as tflite

import os

def create_model():

    print("Initiaizing deep learning model...\n")

    try:

        model = tflite.Interpreter(model_path = os.path.join(os.getcwd(), os.pardir, '.model/model.tflite')) # should be tflite.Interpreter once deployed to Pi
        model.allocate_tensors()
        
    except Exception as e:
        
        model = 0
        print("Error initializing TFLite model...")

    return model

"""
@Citations

This computer program was developed for academic purposes, and has been derived with
the help of several external sources. These external sources are cited here, in IEEE
format:

“Object detection &nbsp;: &nbsp; tensorflow hub,” TensorFlow. [Online]. Available: 
    https://www.tensorflow.org/hub/tutorials/object_detection. [Accessed: 15-Sep-2022]. 

"""
