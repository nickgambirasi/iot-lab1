"""
@Notice of Interaction with Third Party Software

This computer program interacts with various third-party open-source
software under their respective licenses. For more information, see
conda list.

"""

from operator import truediv
from os import path
from advanced_mapping import scan_to_array
from model_labels import get_labels
from model import create_model
from object_detection import set_tensor, output_to_tensor, detect, evaluate_predictions
from path_planning_and_nav import a_star, get_turn_list

import picamera
import picamera.array

import numpy as np

import time

import picar_4wd as fc

import cv2

"""
Intended functionality:

    - Scan the road for obstacles
    - Using that scan, map the path to the destination
    - Create a list of directions to be used when obstacles are encountered
    - While navigating the obstacles in the path with the direction list, check
        camera view for stop signs, and stop if the vehicle detects one

"""

def all_equal(l, value):

    for val in l:

        if val == value:

            continue

        else:

            return False
    
    return True

def main():

    SPEED = 10
    
    # one-time intializations
    # load tflite object detection model
    #model = create_model()
    #model.allocate_tensors()
    #_, width, height, _ = model.get_input_details()[0]['shape']

    # load the labels
    labels = get_labels()

    # initialize the camera
    # with picamera.PiCamera(resolution=(1920, 1080)) as camera:

    #    with picamera.array.PiRGBArray(camera) as output:
        
        # begin the loop of scanning for obstacles, planning the path,
        # and checking for stop signs while following the path

    while True:

        fc.stop()
        
        # scan for obstacles
        mp = scan_to_array(angles=np.arange(-60, 60, 1))

        # calcuate path
        pth = a_star(mp, (0, 50), (99, 50))

        if len(pth) == 0:

            fc.forward(SPEED)
            time.sleep(0.5)
            continue

        directions = get_turn_list(pth)

        num_stops = 0

        #output.truncate(0)

        #camera.capture(output, 'bgr')
        #image = output.array

        #cv2.imshow("live feed", image)

        # convert image to rgb for tflite

        #image_tf = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        #image_tf = cv2.resize(image_tf, (320, 320))

        #n, boxes, classes, scores = detect(model, image_tf, labels)

        #flag = evaluate_predictions(boxes, classes, scores, 0.5)

        #if flag == -1:
            

            #fc.stop()
            #break

        #else:
        while True:

            if num_stops == 2:

                fc.stop()

                break

            fc.forward(SPEED)

            scan_list = fc.scan_step(20)

            if not scan_list:
                continue

            tmp = scan_list[3:7]
            print(tmp)

            if not all_equal(tmp, 2):

                fc.stop()
                num_stops += 1
                time.sleep(3.0)

                direction = directions.pop(0)

                if direction == 'right':

                    fc.turn_right(SPEED)
                    time.sleep(1.5)

                else:

                    fc.turn_left(SPEED)
                    time.sleep(1.6)

if __name__ == '__main__':

    try:

        main()

    finally:

        fc.stop()

if __name__ == '__main__':

    main()