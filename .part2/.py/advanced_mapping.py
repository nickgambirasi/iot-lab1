"""
@Notice of Interaction with Third Party Software

This computer program interacts with various third-party open-source
software under their respective licenses. For more information, see
conda list.
"""

"""
Imports of the picar library are commented out for now because I do
not have the ability to integrate with the picar.
"""

import picar_4wd as fc

import math

import numpy as np

def scan_to_array(angles: list = np.arange(-90, 90, 1)):

    """
    The purpose of this function is to utilize the ultrasonic
    sensor to scan for objects and return a numpy array with
    ones where there are obstacles
    
        - Inputs: The list of angles at which we scan for obstacles
        - Outputs: numpy.ndarray with zeroes for clear
            position and ones where there are obstacles
            
    """

    print("Scanning environment for obstacles...\n")

    def clear_nbhd(x, y, thres=1):

        return [x-thres, y-thres, x+thres, y+thres]

    # initialize the obstacle matrix
    obs_numpy = np.ones(shape=(100, 100), dtype = np.uint8)
    car_row, car_col = 50, 0

    for angle in angles:

        obs_dist = fc.get_distance_at(angle)

        # the most common case will be that the row and column are not perfect integers
        # in which case we will fill in all encompassed elements of the array with ones
        obs_row = int(round(car_col + obs_dist*math.sin(math.radians(90 - angle)), 0))

        obs_col = int(round(car_row - obs_dist*math.cos(math.radians(90 - angle)), 0))

        hood = clear_nbhd(obs_row, obs_col, 2)

        for x in np.arange(hood[0], hood[2], 1):

            for y in np.arange(hood[1], hood[3], 1):

                if x < 0:

                    x = 0

                elif x > 99:

                    x = 99
                
                if y < 0:

                    y = 0

                elif y > 99:

                    y = 99

                obs_numpy[x, y] = 0

    print("Obstacle scanning completed.\n")

    return obs_numpy.T

        






