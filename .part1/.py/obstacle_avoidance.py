import picar_4wd as fc
from random import randint 

speed = 20


def main():
    while True:
        scan_list = fc.scan_step(35)
        if not scan_list:
            continue

        scanned = scan_list[3:7]
        if scanned != [2,2,2,2]:
            direction = randint(0,1)
            if direction:
                fc.turn_right(speed)
            else:
                fc.turn_left(speed)
        else:
            fc.forward(speed)


if __name__ == "main":
    try:
        main()
    finally:
        fc.stop()