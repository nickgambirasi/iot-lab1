# SETUP SHELL FILE

# This setup file does the following:
#
#   1) Installs python3-pip on the Raspberry PI
#   2) Installs python3-opencv on the PI
#   3) Verifies the installation of python3-numpy
#       on the PI
#   4) Uses pip to install the following:
#       a) picamera[array]
#       b) tflite-runtime
#   5) Clones the picar_4wd repository and
#       executes the setup file from the repo
#   6) Executes test cases for different scripts
#       used in the lab...and returns steps to
#       fix the environment

# Step 1: Install python3-pip on the PI
$ echo "\nInstalling python3-pip...\n"
$ sudo apt install python3-pip
$ echo "\nPython3-pip successfully installed.\n"

# Step 2: Install python3-opencv on the PI
$ echo "\nInstalling opencv...\n"
$ sudo apt install python3-opencv
$ echo "\nSuccessfully installed opencv."

# Step 3: Verify numpy installation on the PI
$ echo "\nVerifying numpy installation...\n"
$ sudo apt install python3-numpy
$ echo "\nNumpy installation successfully completed.\n"

# Step 4: Install pip dependencies
$ echo "\nInstalling pip dependencies...\n"
$ sudo pip install -r requirements.txt
$ echo "\nPip dependencies successfully installed.\n"

# Step 5: Clone the picar repository and complete
#   the setup steps
$ cd ..
$ git clone https://github.com/sunfounder/picar-4wd
$ cd picar-4wd
$ sudo python3 setup.py

# Step 6: Prepares and executes tests for car control, 
#   car camera control, and computer vision model 
#   integration
$ echo "\nEnabling user test of picar keyboard control...\n"
$ cd examples
$ sudo python3 keyboard_control.py
$ echo "\nCompleted test of picar keyboard control.\n"

$ echo "\nTesting picamera...\n"
$ raspistill -o "test.jpg"
$ echo "\nCamera testing completed.\n"

$ echo "\nRunning user-developed tests...\n"
$ cd ../.part2/.py
$ sudo python3 tests.py
$ echo "\nUser-developed tests completed successfully!\n"
